class AddRsvpToUserEvents < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      CREATE TYPE rsvp_types AS ENUM ('yes', 'no', 'maybe');
    SQL
    add_column :user_events, :rsvp, :rsvp_types
  end

  def down
    remove_column :user_events, :rsvp
    execute <<-SQL
      DROP TYPE rsvp_types
    SQL
  end
  
end
