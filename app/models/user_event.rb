class UserEvent < ApplicationRecord
  enum rsvp: {maybe:'maybe', yes: 'yes', no: 'no'}

  belongs_to :user
  belongs_to :event

  validates :rsvp, presence: true
end
