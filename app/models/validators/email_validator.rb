class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, _value)
    return if record[attribute]&.match?(/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/)
    record.errors.add(attribute, 'Invalid email format')
  end
end