class Event < ApplicationRecord

	has_many :user_events
	has_many :users, through: :user_events

	validates :title, presence: true
	validates :starttime, presence: true
	validates :description, presence: true
	validates :endtime, presence: true, if: :not_fullday_event


	def not_fullday_event
		is_allday == false
	end
	
end
