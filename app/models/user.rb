class User < ApplicationRecord
	has_many :user_events
	has_many :events, through: :user_events

	validates :username, presence: true, uniqueness: true, allow_blank: false
	validates :email, presence: true, uniqueness: true, allow_blank: false, email: true
	

end
