module Seeds
	class EventCsvProcessor < BaseCsvProcessor

		def process!
			ActiveRecord::Base.transaction do
				load_events
				user_event_ids = load_user_events
				user_ids = UserEvent.where(id: user_event_ids.ids).pluck(:user_id)
				marks_rsvp_overlappings(user_ids)
			end
	  end
    
		
		private
		
		def load_events
			events_list = []
			CSV.foreach(csv_file, headers: true) do |event_raw|
					events_list << {title: event_raw['title'],
										starttime: event_raw['starttime']&.to_datetime,
										endtime: event_raw['endtime']&.to_datetime,
										description: event_raw['description'],
										is_allday: event_raw['allday'],
										is_completed: completed?(event_raw['endtime'])}
			end
			Event.import(events_list)
		end

		def load_user_events
			user_events = []
			CSV.foreach(csv_file, headers: true) do |event_raw|
				event_id = Event.find_by(title: event_raw['title'],
									starttime: event_raw['starttime']&.to_datetime,
									endtime: event_raw['endtime']&.to_datetime,
									description: event_raw['description'],
									is_allday: event_raw['allday'],
									is_completed: completed?(event_raw['endtime']))&.id
				next if !event_raw["users#rsvp"].present?
				users_rsvps = event_raw["users#rsvp"].split(%r{\;\s*})
				users_rsvps.each do |user|
					username, rsvp = user.split("#")
					user_id = User.find_by(username: username)&.id
					user_events << {user_id: user_id, event_id: event_id, rsvp: rsvp}
				end
			end
			UserEvent.import(user_events)
		end

		def marks_rsvp_overlappings(user_ids)
			User.where(id: user_ids).includes(:events).find_each do |user|
				previous_event = nil
				user.events.where(is_allday: false).order(:starttime).each do |event|
					if previous_event.nil?
						previous_event = event
						next
					end
					# Compare if any overlaps occured with previous one
					if(previous_event.endtime > event.starttime )
						# select the event whoes enddate is greater as previous event and mark accordingly
						if (previous_event.endtime > event.endtime)
							previous_event.user_events.find_by(user_id: user.id).update(rsvp: "yes")
							event.user_events.find_by(user_id: user.id).update(rsvp: "no")
						else
							previous_event.user_events.find_by(user_id: user.id).update(rsvp: 'no')
							event.user_events.find_by(user_id: user.id).update(rsvp: "yes")
							previous_event = event
						end
					else
						previous_event = event
					end
				end
			end
		end
		
		def completed?(date)
			date.to_datetime < Time.zone.now
		end
	
	end
end