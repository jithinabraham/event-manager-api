module Seeds
	class UserCsvProcessor < BaseCsvProcessor
		
		def process!
			users = []
			CSV.foreach(csv_file, headers: true) do |row|
					users << row.to_h
			end
			User.import(users)
		end

	end
end