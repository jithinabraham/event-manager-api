class EventsController < ApplicationController

	def search
		start_date = search_params[:start_date].to_date.beginning_of_day
		end_date = search_params[:start_date].to_date.end_of_day
		@events = Event.where("starttime <= ? AND ? <= endtime", end_date, start_date)
		render json: @events
	end

	def user_availability
		user = User.find user_availability_params[:user_id]
		start_date = search_params[:start_date].to_date
		end_date = search_params[:start_date].to_date
		
		event_dates = Event.where("starttime <= ? AND ? <= endtime", end_date.end_of_day, start_date.beginning_of_day)
									.pluck('starttime,endtime').flatten.map(&:to_date).uniq
		@available_dates = (start_date..end_date).to_a - event_dates
		render json: {available_dates: @available_dates}
	end

	def search_params
		params.require(:events).permit(:start_date, :end_date)
	end

	def user_availability_params
		params.require(:events).permit(:start_date, :end_date, :user_id)
	end

end
