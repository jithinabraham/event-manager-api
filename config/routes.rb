Rails.application.routes.draw do
 resources :events, only: [] do
   post :search, on: :collection
   post :user_availability, on: :collection
 end
end
