# README

* Ruby version - 2.6.3

* Database  - postgreSQL

* Load data from csv
    * `rake db:create db:migrate db:seed`   

* Api Documentation - https://documenter.getpostman.com/view/3449931/SWTABymz?version=latest
